/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */

#include <SPI.h>
#include <WiFi101.h>
#include "Arduino.h"
#include "CooperativeMultitasking.h"
#include "MQTTClient.h"

const char ssid[] = "...";
const char pass[] = "...";
const char host[] = "broker.hivemq.com";
const char clientid[] = "...";
const char username[] = "...";
const char password[] = "...";
const char topicfilter[] = "demo45520/test";

CooperativeMultitasking tasks;
WiFiClient wifiClient;
MQTTClient mqttClient(&tasks, &wifiClient);

bool beginWiFi() {
    if (WiFi.status() == WL_CONNECTED) return true;
    //
    WiFi.begin(ssid, pass);
    //
    return false;
}

void ledOn() {
    digitalWrite(LED_BUILTIN, HIGH);
}

void ledOff() {
    digitalWrite(LED_BUILTIN, LOW);
}

void onMessage(const PublishNotification* notification) {
    tasks.now(ledOff);
    tasks.after(3000, ledOn);
}

void setup() {
    Serial.begin(115200);
    //
    if (!Serial) {
        delay(1000);
    }
    //
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    WiFi.begin(ssid, pass);
    mqttClient.setHost(host);
    mqttClient.setClientId(clientid);
    mqttClient.setUsernamePassword(username, password);
    mqttClient.setNetworkInitializer(beginWiFi);
    mqttClient.setSubscription(topicfilter, onMessage);
    mqttClient.setConnectListener(ledOn);
    mqttClient.setDisconnectListener(ledOff);
    mqttClient.setLogLevel(2); // info
    mqttClient.begin();
}

void loop() {
    if (tasks.available() > 0) {
        tasks.run();
    }
}
