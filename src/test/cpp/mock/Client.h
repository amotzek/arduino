#ifndef Client_h
#define Client_h

#include <cstdint>
#include <cstddef>

class Client {
    private:
        int fd;
        bool writeError;

    public:
        bool connect(const char* host, uint16_t port);
        size_t available();
        int read();
        void write(uint8_t* buffer, size_t len);
        void flush();
        bool getWriteError() { return writeError; }
        void clearWriteError() { writeError = false; }
        void stop();
};

#endif
