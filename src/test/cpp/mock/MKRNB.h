#ifndef MKRNB_h
#define MKRNB_h
#include "Client.h"

#define NB_READY 1
#define GPRS_READY 1

class NBClient : public Client {
};

class GPRS {
    public:
        int attachGPRS() { return GPRS_READY; }
};

class NB {
    public:
        int begin(const char* pinnumber) { return NB_READY; }
};

#endif
