#ifndef WiFi_h
#define WiFi_h

#include "Client.h"

#define WL_CONNECTED 1

class WiFiClass {
    public:
        int status() { return WL_CONNECTED; }
        void begin(const char* ssid, const char* pwd) { }
};

class WiFiClient : public Client {
};

extern WiFiClass WiFi;

#endif
