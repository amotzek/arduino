#ifndef Arduino_h
#define Arduino_h

#include <cstdint>
#include <cstddef>
#include <cstring>
#include <cmath>
#include <string>
#include <sstream>

#define LED_BUILTIN 13
#define HIGH true
#define LOW false
#define OUTPUT true
#define INPUT false

using std::min;

using std::max;

float abs(float a);

float degrees(float a);

class String {
    private:
        std::string contents;

    public:
        String(const char* val) { contents = std::string(val); }
        String(float val) { std::ostringstream stream; stream << val; contents = std::string(stream.str()); }
        ~String() { }
        size_t length() const { return contents.length(); }
        const std::string getStdString() const { return contents; }
        const char* c_str() const { return contents.c_str(); }
        void operator += (const String& val) { contents += val.contents; }
};

class SerialClass {
    public:
        SerialClass() { }
        void begin(uint32_t baudrate) { }
        size_t availableForWrite() { return 65536; }
        void print(String str);
        void println(String str);
        void flush() { }
        operator bool() { return true; }
};

extern SerialClass Serial;

void delay(long ms);

long millis();

void digitalWrite(int pin, bool val);

void pinMode(int pin, bool mode);

void setup();

void loop();

int main();

#endif
