#include "Arduino.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#include "time.h"

float abs(float a) {
    if (a >= 0.0) return a;
    //
    return -a;
}

float degrees(float a) {
    return a * 360.0f / 3.14159265f;
}

SerialClass Serial;

void SerialClass::print(String str) {
    std::cout << str.getStdString();
}

void SerialClass::println(String str) {
    std::cout << str.getStdString() << std::endl;
}

void delay(long ms) {
    if (ms > 0l) {
        timespec ts;
        ts.tv_sec = ms / 1000l;
        ts.tv_nsec = 1000000 * (ms % 1000);
        nanosleep(&ts, nullptr);
    }
}

long millis() {
    timespec ts;
    //
    if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0) {
        return 1000l * ts.tv_sec + (ts.tv_nsec / 1000000ul);
    }
    //
    return 0l;
}

void pinMode(int pin, bool mode) {
}

void digitalWrite(int pin, bool val) {
    std::cout << "OUTP " << pin << " " << val << std::endl;
}

int main() {
    char* duration = getenv("DURATION");
    setup();
    //
    if (duration) {
        long until = millis() + std::stol(duration);
        //
        while (millis() < until) {
            loop();
        }
    } else {
        while (true) {
            loop();
        }
    }
    //
    return 0;
}
