#ifndef Arduino_LSM6DS3_h
#define Arduino_LSM6DS3_h

class IMUClass {
    public:
        bool begin() { return true; }
        bool accelerationAvailable() { return true; }
        void readAcceleration(float& x, float& y, float& z);
};

extern IMUClass IMU;

#endif
