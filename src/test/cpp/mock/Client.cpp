#include "Client.h"
#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#include "time.h"

bool Client::connect(const char* hostname, uint16_t port) {
    // https://stackoverflow.com/questions/52727565/client-in-c-use-gethostbyname-or-getaddrinfo
    struct addrinfo hints = {}, *addrs;
    std::string port_str = std::to_string(port);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    //
    if (getaddrinfo(hostname, port_str.c_str(), &hints, &addrs) != 0) {
        return false;
    }
    //
    for (struct addrinfo *addr = addrs; addr != NULL; addr = addr->ai_next) {
        fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        //
        if (fd < 0) {
            continue;
        }
        //
        if (::connect(fd, addr->ai_addr, addr->ai_addrlen) == 0) {
            freeaddrinfo(addrs);
            //
            return true;
        }
        //
        close(fd);
    }
    //
    freeaddrinfo(addrs);
    //
    return false;
}

size_t Client::available() {
    // https://stackoverflow.com/questions/14047802/how-to-check-amount-of-data-available-for-a-socket-in-c-and-linux
    int count = 0;
    //
    if (ioctl(fd, FIONREAD, &count) < 0) {
        return 0;
    }
    //
    return count;
}

int Client::read() {
    uint8_t data[4];
    auto res = ::read(fd, data, 1);
    //
    if (res <= 0) {
        return -1;
    }
    //
    return data[0];
}

void Client::write(uint8_t* buffer, size_t size) {
    auto res = ::write(fd, buffer, size);
    //
    if (res < 0) {
        writeError = true;
    }
    //
    if ((size_t) res != size) {
        writeError = true;
    }
}

void Client::flush() {
}

void Client::stop() {
    if (fd >= 0) {
        close(fd);
    }
    //
    fd = -1;
}
