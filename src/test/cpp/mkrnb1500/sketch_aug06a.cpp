/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */

#include <MKRNB.h>
#include "CooperativeMultitasking.h"
#include "MQTTClient.h"

const char pinnumber[] = "..."; // enter your SIM pin here
const char host[] = "broker.hivemq.com";
const char clientid[] = "..."; // enter an unique client id here
const char username[] = "...";
const char password[] = "...";
const char topicfilter[] = "demo45520/test"; // enter your topic filter here

CooperativeMultitasking tasks;
NBClient client;
GPRS gprs;
NB narrowband;
MQTTClient mqttClient(&tasks, &client);

bool beginGprs() {
    return narrowband.begin(pinnumber) == NB_READY && gprs.attachGPRS() == GPRS_READY;
}

void ledOn() {
    digitalWrite(LED_BUILTIN, HIGH);
}

void ledOff() {
    digitalWrite(LED_BUILTIN, LOW);
}

void onMessage(const PublishNotification* notification) {
    if (!notification->isDuplicate()) {
        tasks.now(ledOff);
        tasks.after(3000, ledOn);
    }
}

void setup() {
    Serial.begin(115200);
    //
    if (!Serial) {
        delay(1000);
    }
    //
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    mqttClient.setHost(host);
    mqttClient.setClientId(clientid);
    mqttClient.setUsernamePassword(username, password);
    mqttClient.setNetworkInitializer(beginGprs);
    mqttClient.setSubscription(topicfilter, onMessage);
    mqttClient.setConnectListener(ledOn);
    mqttClient.setDisconnectListener(ledOff);
    mqttClient.setLogLevel(2); // info
    mqttClient.begin();
}

void loop() {
    if (tasks.available() > 0) {
        tasks.run();
    }
}
