/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */

#include <SPI.h>
#include <WiFiNINA.h>
#include <Arduino_LSM6DS3.h>
#include "Arduino.h"
#include "CooperativeMultitasking.h"
#include "MQTTClient.h"

const char ssid[] = "...";
const char pass[] = "...";
const char host[] = "broker.hivemq.com";
const char clientid[] = "...";
const char username[] = "...";
const char password[] = "...";
const char topicfilter[] = "demo45520/test";
const char topic[] = "demo45519/test";

CooperativeMultitasking tasks;
WiFiClient wifiClient;
MQTTClient mqttClient(&tasks, &wifiClient);

bool queryMeasurement = true;
float lastRadius = 0.0f;
float lastAngle = 0.0f;

bool beginWiFi() {
    if (WiFi.status() == WL_CONNECTED) return true;
    //
    WiFi.begin(ssid, pass);
    //
    return false;
}

void ledOn() {
    digitalWrite(LED_BUILTIN, HIGH);
}

void ledOff() {
    digitalWrite(LED_BUILTIN, LOW);
}

void onMessage(const PublishNotification* notification) {
    if (!notification->isDuplicate()) {
        tasks.now(ledOff);
        tasks.after(3000, ledOn);
        queryMeasurement = true;
    }
}

bool radiusChanged(float r1, float r2) {
    float r = max(r1, r2);
    //
    if (r < 0.001f) return false;
    //
    float delta = abs(r1 - r2);
    float limit = max(0.001f * r, 0.003f);
    //
    return delta > limit;
}

bool angleChanged(float a1, float a2, float r1, float r2) {
    float r = max(r1, r2);
    //
    if (r < 0.001f) return false;
    //
    float delta = min(abs(a1 - a2), 360.0f - abs(a1 - a2));
    float limit = 0.36f / r;
    //
    return delta > limit;
}

void publishRadiusAndAngle() {
    if (IMU.accelerationAvailable()) {
        float x, y, z;
        IMU.readAcceleration(x, y, z);
        float radius = sqrt(x * x + y * y);
        float angle = degrees(atan2(y, x));
        //
        if (queryMeasurement || radiusChanged(radius, lastRadius) || angleChanged(angle, lastAngle, radius, lastRadius)) {
            String payload = "{\"::gradient-polar-radius\": ";
            payload += String(radius);
            payload += ", \"::gradient-polar-angle\": ";
            payload += String(angle);
            payload += "}";
            mqttClient.publish(topic, payload.c_str());
            lastRadius = radius;
            lastAngle = angle;
            queryMeasurement = false;
        }
        //
        tasks.after(1000, publishRadiusAndAngle);
    }
}

void setup() {
    Serial.begin(115200);
    //
    if (!Serial) {
        delay(1000);
    }
    //
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    WiFi.begin(ssid, pass);
    IMU.begin();
    mqttClient.setHost(host);
    mqttClient.setClientId(clientid);
    mqttClient.setUsernamePassword(username, password);
    mqttClient.setNetworkInitializer(beginWiFi);
    mqttClient.setSubscription(topicfilter, onMessage);
    mqttClient.setConnectListener(ledOn);
    mqttClient.setDisconnectListener(ledOff);
    mqttClient.setLogLevel(2); // info 2, warning 1, no log 0
    mqttClient.begin();
    tasks.now(publishRadiusAndAngle);
}

void loop() {
    if (tasks.available() > 0) {
        tasks.run();
    }
}
