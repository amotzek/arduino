/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the MQTT Client package.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */

#ifndef MQTTClient_h
#define MQTTClient_h

#include "CooperativeMultitasking.h"
#include "MQTTSocket.h"

class MQTTClient {
    private:
        CooperativeMultitasking* tasks;
        MQTTSocket* socket;
        const char* host;
        uint16_t port;
        const char* clientId;
        const char* username;
        const char* password;
        unsigned long connectInterval;
        unsigned long responseTimeout;
        unsigned long retryInterval;
        unsigned long pingInterval;
        std::function<bool()> networkInitializer;
        const char* topicFilter;
        std::function<void(const PublishNotification*)> notificationListener;
        std::function<void()> connectListener;
        std::function<void()> disconnectListener;
        bool begun;
        char* topic;
        char* payload;
        bool retain;
        uint16_t publishedpacketid;
        bool duplicate;
        uint8_t loglevel;
        void info(String str);
        void warning(String str);
        void initializeNetwork();
        void connectSocket();
        bool canReadSocket();
        bool canPublishOriginal();
        bool canPublishDuplicate();
        void sendConnect();
        void acknowledgeConnect();
        void subscribe();
        void receive();
        void publish1();
        void ping();
        void select();
        void closeSocket();

    public:
        MQTTClient(CooperativeMultitasking* tasks, Client* client);
        virtual ~MQTTClient();
        bool setHost(const char host[]);
        bool setPort(uint16_t port);
        bool setClientId(const char clientId[]);
        bool setUsernamePassword(const char username[], const char password[]);
        bool setConnectInterval(unsigned long interval);
        bool setResponseTimeout(unsigned long timeout);
        bool setRetryInterval(unsigned long interval);
        bool setPingInterval(unsigned long interval);
        bool setNetworkInitializer(std::function<bool()> initializer);
        bool setSubscription(const char topicFilter[], std::function<void(const PublishNotification*)> listener);
        bool setConnectListener(std::function<void()> listener);
        bool setDisconnectListener(std::function<void()> listener);
        void setLogLevel(uint8_t loglevel); // 0 no logging, 1 warning, 2 info
        bool begin();
        bool publish(const char topic[], const char payload[], bool retain = true, bool force = true);
};

#endif
