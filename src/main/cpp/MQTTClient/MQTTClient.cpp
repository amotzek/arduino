/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the MQTT Client package.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */

#include "MQTTClient.h"

MQTTClient::MQTTClient(CooperativeMultitasking* t, Client* c) {
    tasks = t;
    socket = new MQTTSocket(c);
    host = nullptr;
    port = 1883;
    clientId = nullptr;
    username = nullptr;
    password = nullptr;
    connectInterval = 30000;
    responseTimeout = 5000;
    retryInterval = 5000;
    pingInterval = 300000;
    topicFilter = nullptr;
    networkInitializer = nullptr;
    notificationListener = nullptr;
    connectListener = nullptr;
    disconnectListener = nullptr;
    begun = false;
    topic = nullptr;
    payload = nullptr;
    retain = false;
    publishedpacketid = 0;
    duplicate = false;
    loglevel = 0;
}

MQTTClient::~MQTTClient() {
    delete socket;
}

bool MQTTClient::setHost(const char h[]) {
    if (begun) return false;
    //
    host = h;
    //
    return true;
}

bool MQTTClient::setPort(uint16_t p) {
    if (begun) return false;
    //
    port = p;
    //
    return true;
}

bool MQTTClient::setClientId(const char ci[]) {
    if (begun) return false;
    //
    clientId = ci;
    //
    return true;
}

bool MQTTClient::setUsernamePassword(const char u[], const char p[]) {
    if (begun) return false;
    //
    username = u;
    password = p;
    //
    return true;
}

bool MQTTClient::setConnectInterval(unsigned long ci) {
    if (ci < 1000) return false;
    //
    connectInterval = ci;
    //
    return true;
}

bool MQTTClient::setResponseTimeout(unsigned long rt) {
    if (rt < 1000) return false;
    //
    responseTimeout = rt;
    //
    return true;
}

bool MQTTClient::setRetryInterval(unsigned long rt) {
    if (rt < 1000) return false;
    //
    retryInterval = rt;
    //
    return true;
}

bool MQTTClient::setPingInterval(unsigned long pi) {
    if (pi < 1000) return false;
    //
    pingInterval = pi;
    //
    return true;
}

bool MQTTClient::setNetworkInitializer(std::function<bool()> ni) {
    if (!ni) return false;
    //
    networkInitializer = ni;
    //
    return true;
}

bool MQTTClient::setSubscription(const char t[], std::function<void(const PublishNotification*)> nl) {
    if (begun) return false;
    //
    topicFilter = t;
    notificationListener = nl;
    //
    return true;
}

bool MQTTClient::setConnectListener(std::function<void()> cl) {
    connectListener = cl;
    //
    return true;
}

bool MQTTClient::setDisconnectListener(std::function<void()> dl) {
    disconnectListener = dl;
    //
    return true;
}

bool MQTTClient::begin() {
    if (begun) return true;
    //
    if (!host || !networkInitializer) return false;
    //
    begun = tasks->now(std::bind(&MQTTClient::initializeNetwork, this));
    //
    return begun;
}

bool MQTTClient::publish(const char t[], const char p[], bool r, bool force) {
    if (!t || !p) return false;
    //
    if (force) {
        free(topic);
        free(payload);
        topic = nullptr;
        payload = nullptr;
    }
    //
    if (topic || payload) return false;
    //
    topic = strdup(t);
    payload = strdup(p);
    retain = r;
    publishedpacketid = 0;
    duplicate = false;
    //
    return topic && payload;
}

void MQTTClient::setLogLevel(uint8_t l) {
    loglevel = l;
}

void MQTTClient::info(String str) {
    if (loglevel < 2 || !Serial) return;
    //
    if (Serial.availableForWrite() < str.length()) Serial.flush();
    //
    Serial.print("INFO ");
    Serial.println(str);
}

void MQTTClient::warning(String str) {
    if (loglevel < 1 || !Serial) return;
    //
    Serial.print("WARN ");
    Serial.println(str);
    Serial.flush();
}

void MQTTClient::initializeNetwork() {
    if (networkInitializer()) {
        info("network initialized");
        tasks->after(300, std::bind(&MQTTClient::connectSocket, this));
    } else {
        warning("cannot initialize network");
        tasks->after(connectInterval, std::bind(&MQTTClient::initializeNetwork, this));
    }
}

void MQTTClient::connectSocket() {
    if (socket->connect(host, port)) {
        info("socket connected");
        tasks->after(300, std::bind(&MQTTClient::sendConnect, this));
    } else {
        warning("cannot connect socket");
        tasks->after(connectInterval, std::bind(&MQTTClient::initializeNetwork, this));
    }
}

bool MQTTClient::canReadSocket() {
    return socket->canReadSocket();
}

bool MQTTClient::canPublishOriginal() {
    return topic && payload && !duplicate;
}

bool MQTTClient::canPublishDuplicate() {
    return topic && payload && duplicate;
}

void MQTTClient::sendConnect() {
    info("before connect request");
    //
    if (socket->sendConnectRequest(clientId, username, password, pingInterval >> 9)) {
        info("sent connect request");
        auto task1 = tasks->whenThen(std::bind(&MQTTClient::canReadSocket, this), std::bind(&MQTTClient::acknowledgeConnect, this));
        auto task2 = tasks->after(responseTimeout, std::bind(&MQTTClient::closeSocket, this));
        tasks->onlyOneOf(task1, task2);
    } else {
        warning("cannot send connect request");
        tasks->now(std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::acknowledgeConnect() {
    info("before receive");
    auto packet = socket->receive();
    //
    if (packet) {
        if (packet->getType() == 2) {
            auto acknowledgement = (ConnectAcknowledgement*) packet;
            //
            if (acknowledgement->isConnectionAccepted()) {
                info("connection accepted");
                //
                if (topicFilter) {
                    tasks->after(300, std::bind(&MQTTClient::subscribe, this));
                } else {
                    tasks->now(connectListener, 1);
                    select();
                }
            } else {
                warning("connection not accepted");
                tasks->now(std::bind(&MQTTClient::closeSocket, this));
            }
        } else {
            warning("unexpected packet in acknowledge connect");
            tasks->now(std::bind(&MQTTClient::closeSocket, this));
        }
        //
        delete packet;
    } else {
        warning("no packet in acknowledge connect");
        tasks->now(std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::subscribe() {
    info("before subscribe request");
    //
    if (socket->sendSubscribeRequest(topicFilter, 1)) {
        info("sent subscribe request");
        auto task1 = tasks->whenThen(std::bind(&MQTTClient::canReadSocket, this), std::bind(&MQTTClient::receive, this));
        auto task2 = tasks->after(responseTimeout, std::bind(&MQTTClient::closeSocket, this));
        tasks->onlyOneOf(task1, task2);
    } else {
        warning("cannot send subscribe request");
        tasks->now(std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::receive() {
    info("before receive");
    auto packet = socket->receive();
    //
    if (packet) {
        switch (packet->getType()) {
            case 3: {
                auto notification = (PublishNotification*) packet;
                info("publish notification");
                info(notification->getPayload());
                //
                if (notification->getPacketId() != 0) {
                    info("before publish acknowledgement");
                    //
                    if (socket->sendPublishAcknowledgement(notification->getPacketId())) {
                        info("sent publish acknowledgement");
                    } else {
                        warning("cannot send publish acknowledgement");
                    }
                }
                //
                if (notificationListener) {
                    info("before invoke notification listener");
                    notificationListener(notification);
                    info("invoked notification listener");
                }
                //
                break;
            }
            case 4: {
                auto acknowledgement = (PublishAcknowledgement*) packet;
                //
                if (acknowledgement->hasPacketId(publishedpacketid)) {
                    info("publish acknowledged");
                    free(topic);
                    free(payload);
                    topic = nullptr;
                    payload = nullptr;
                    publishedpacketid = 0;
                    duplicate = false;
                }
                //
                break;
            }
            case 9: {
                auto acknowledgement = (SubscribeAcknowledgement*) packet;
                //
                if (acknowledgement->hasPacketId(socket->getPacketId()) && acknowledgement->isSubscriptionAccepted()) {
                    info("subscription accepted");
                    tasks->now(connectListener, 1);
                } else {
                    warning("wrong packet id or subscription not accepted");
                }
                //
                break;
            }
            case 13: {
                info("ping response");
                //
                break;
            }
        }
        //
        delete packet;
    } else {
        warning("cannot receive packet");
    }
    //
    if (socket->isReadComplete() && socket->isWriteComplete()) {
        select();
    } else {
        tasks->after(5000, std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::publish1() {
    info("before publish request");
    //
    if (socket->sendPublishRequest(topic, payload, retain, duplicate)) {
        info("sent publish request");
        info(topic);
        info(payload);
        publishedpacketid = socket->getPacketId();
        duplicate = true;
        select();
    } else {
        warning("cannot send publish request");
        tasks->now(std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::select() {
    auto task1 = tasks->whenThen(std::bind(&MQTTClient::canReadSocket, this), std::bind(&MQTTClient::receive, this));
    auto task2 = tasks->whenThen(std::bind(&MQTTClient::canPublishOriginal, this), std::bind(&MQTTClient::publish1, this));
    auto task3 = tasks->whenForThen(std::bind(&MQTTClient::canPublishDuplicate, this), retryInterval, std::bind(&MQTTClient::publish1, this));
    auto task4 = tasks->after(pingInterval, std::bind(&MQTTClient::ping, this));
    tasks->onlyOneOf(task1, task2, task3, task4);
}

void MQTTClient::ping() {
    info("before ping request");
    //
    if (socket->sendPingRequest()) {
        info("sent ping request");
        auto task1 = tasks->whenThen(std::bind(&MQTTClient::canReadSocket, this), std::bind(&MQTTClient::receive, this));
        auto task2 = tasks->after(responseTimeout, std::bind(&MQTTClient::closeSocket, this));
        tasks->onlyOneOf(task1, task2);
    } else {
        warning("cannot send ping request");
        tasks->now(std::bind(&MQTTClient::closeSocket, this));
    }
}

void MQTTClient::closeSocket() {
    info("close socket");
    socket->close();
    tasks->now(disconnectListener, 1);
    tasks->after(connectInterval, std::bind(&MQTTClient::connectSocket, this));
}
